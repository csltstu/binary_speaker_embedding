#!/bin/bash

dim_raw_vector=$1       # e.g., 150
dim_binary_vector=$2    # e.g., 150
dir_LSH_matrix=$3      # e.g., matrix/
file_raw_vector=$4      # e.g., raw/raw_lda_vector.ark
file_binary_vector=$5   # e.g., binary/binary_150.ark

# init LSH matrix
python mapBinaryHamming.py $1 $2 $3

# binary vector extraction
python mergeBinaryHamming.py $4 $3 $1 > $5
