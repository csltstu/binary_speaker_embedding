#!/nfs/disk/perm/linux-x86_64/bin/python

import numpy as np
import sys, random, math
import os

def init_model(batch_size, batch_binary_size):
	return np.random.normal(0.0, 1e-3, (batch_size, batch_binary_size)) / math.sqrt(batch_size)

if __name__ == "__main__":
	batch_size = int(sys.argv[1])         # dimension of raw i-vector
	batch_binary_size = int(sys.argv[2])  # dimension of binary vector
	output_path = sys.argv[3]             # dir of LSH matrix
	sys.stderr.write("Begin to Init model .......\n")
	W_list = init_model(batch_size, batch_binary_size)
	if not os.path.exists(output_path):
		os.mkdir(output_path)
	output_file = file(output_path + "/W", 'w')
	for j in range(W_list.shape[0]):
		for k in range(W_list.shape[1]):
			output_file.write(str(W_list[j][k]) + str(' '))
		output_file.write('\n')			
	sys.stderr.write("Init done !\n")
