#!/nfs/disk/perm/linux-x86_64/bin/python

import numpy as np
import sys, random, math, os
import pdb
from scipy.spatial import distance
from math import sqrt

def loadDict(Path):
	input_file = file(Path)
	temp_dict = {}
	for line in input_file:		
		line = line.strip()
		wordList = line.split(' ')
		if len(wordList) <= 2:
			continue
		temp_dict[wordList[0]] = np.array(wordList[1:], dtype = float)
		temp_dict[wordList[0]] /= np.linalg.norm(temp_dict[wordList[0]])
	return temp_dict

def loadMatrix(matrix_path):
	input_file = file(matrix_path)
	temp_list = []
	for line in input_file:
		line = line.strip()
		wordList = line.split(' ')
		temp_list.append(np.array(wordList, dtype = float))
	return temp_list


def loadMatrixList(matrix_path):
	W_name_list = os.listdir(matrix_path)
	temp_list = []
	temp_pair_list = []
	
	sorted_list = []

	for W_name in W_name_list:
		try:
			W_pair = W_name.split('_')
			sorted_list.append([W_pair[0], [int(W_pair[1]), int(W_pair[2])]])
		except:
			sys.stderr.write(W_name + "\n")
	sorted_list = sorted(sorted_list, key = lambda x:x[1][0])

	for W_list in sorted_list:
		temp_pair_list.append(W_list[1])
		temp_list.append(loadMatrix(matrix_path + '/' + W_list[0] + '_' + str(W_list[1][0]) + '_' + str(W_list[1][1])))
	return temp_list, temp_pair_list


def convert(vector):
	temp_vector = np.zeros((vector.shape[0]), dtype = int)
	for i in range(vector.shape[0]):
		if vector[i] > 0:
			temp_vector[i] = 1
		else:
			temp_vector[i] = 0
	return temp_vector


def generateBinaryDict(vectors_dict, W_list, dim_pair_list):
	temp_dict = {}
	for word, vector in vectors_dict.items():
		vector_list = []
		for i in range(len(W_list)):
			W = W_list[i]		
			vector_list = list(convert(np.dot(vector[dim_pair_list[i][0]:dim_pair_list[i][1]], W)))
		temp_dict[word] = np.array(vector_list, dtype = int)
	return temp_dict


def generateBinary(vectors_dict, W, batch_size):
	temp_dict = {}	
	for word, vector in vectors_dict.items():
		vector_list = np.dot(vector[0:batch_size], W)		
		temp_dict[word] = np.array(vector_list, dtype = float)
	return temp_dict


def mergeModel(vector_path, matrix_path, binary_count = 1, batch_size = 2):
	#source parameter init
	iter = 0
	sys.stderr.write("Load Dict...\n")
	vectors_dict = loadDict(vector_path)
	W = loadMatrix(matrix_path)
	W_list = [W for i in range(binary_count)]
	dim_pair_list = [[i * batch_size, (i + 1) * batch_size] for i in range(binary_count)]
	
	sys.stderr.write("Load Dict done !\n")
	vector_list = vectors_dict.values()
	vector_list = np.array(vector_list)

	binary_dict = generateBinaryDict(vectors_dict, W_list, dim_pair_list)

	for word, vector in binary_dict.items():
		print word + ' ' + ' '.join(list(np.array(vector, dtype = 'str')))

if __name__ == "__main__":
	vector_path = sys.argv[1]          # file path of raw i-vector
	matrix_path = sys.argv[2]          # dir of LSH matrix
	matrix_path = matrix_path + '/W'
	binary_size = int(sys.argv[3])     # dimension of raw i-vector
	binary_count = 1
	sys.stderr.write("Begin to merge model .......\n")
	mergeModel(vector_path = vector_path, matrix_path = matrix_path, binary_count = binary_count, batch_size = binary_size)
	sys.stderr.write(matrix_path + " Merge done !\n")
